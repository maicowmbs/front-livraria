'use strict';

var gulp = require('gulp');
var gutil = require('gulp-util');
var wrench = require('wrench');
var gulpNgConfig = require('gulp-ng-config');

var BUILD_PRD = "producao",
    BUILD_HMG = "homologacao",
    BUILD_LCL = "local";

var jsSetting = BUILD_LCL;

var options = {
  src: 'src',
  dist: 'dist',
  tmp: '.tmp',
  e2e: 'e2e',
  errorHandler: function(title) {
    return function(err) {
      gutil.log(gutil.colors.red('[' + title + ']'), err.toString());
      this.emit('end');
    };
  },
  wiredep: {
    directory: 'bower_components',
    exclude: [/bootstrap-sass\/.*\.js/, /bootstrap\.css/]
  }
};

wrench.readdirSyncRecursive('./gulp').filter(function(file) {
  return (/\.(js|coffee)$/i).test(file);
}).map(function(file) {
  require('./gulp/' + file)(options);
});

gulp.task('default', ['clean'], function () {
    gulp.start('build');
});


//Configura constante para API
//onde --setting recebe parametros: producao, homologacao, local
if (gutil.env.setting === BUILD_PRD) {
jsSetting = BUILD_PRD;
} else if (gutil.env.setting === BUILD_HMG) {
jsSetting = BUILD_HMG;
} else if (gutil.env.setting === BUILD_LCL) {
jsSetting = BUILD_LCL;
}

gulp.src('configFile.json')
.pipe(gulpNgConfig('app', {
                      environment: jsSetting,
                      createModule: false
                    }))
.pipe(gulp.dest('./src/app/generics/constants'))

gulp.task('default', ['clean', 'settingsAmb'], function () {
    gulp.start('build');
});