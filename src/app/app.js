'use strict';

angular.module('app', [
    'ngAnimate',
    'ngResource',
    'ui.bootstrap',
    'ui.router',
    'ngStorage',
    'underscore'
])
.config(['$stateProvider', '$urlRouterProvider',
function ($stateProvider, $urlRouterProvider) {
    $urlRouterProvider.otherwise('/loja');
    $stateProvider
        .state('app', {
            abstract: true,
            templateUrl: 'app/app.html'
        })
        .state('app.loja', {
            url: '/loja',
            templateUrl: 'app/components/pages/loja/loja.html',
            controller: 'LojaController'
        })
        .state('app.carrinho', {
            url: '/carrinho/:carrinho',
            templateUrl: 'app/components/pages/carrinho/carrinho.html',
            controller: 'CarrinhoController'
        });

}]);
