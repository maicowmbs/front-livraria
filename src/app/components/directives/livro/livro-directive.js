"use strict";

angular.module("app")
.directive('livroDirective', function(){
        return {
            restrict: 'EA', // elemento ou atributo
            transclude: true,
            replace: true,
            templateUrl: 'app/components/directives/livro/livro.html',
            scope: {
                livro: '=',
                adicionaLivro: '&'
            },
            link: function(scope, elm, attrs) {
            }
          };
    })