"use strict";

angular.module("app").controller("LojaController", [
  "$scope",
  "$rootScope",
  "$state",
  "HTTPGenericService",
  "CarrinhoService",
  function($scope, $rootScope, $state, HTTPGenericService, CarrinhoService) {

    $scope.livros = {};

    $scope.livrosCarrinhoTotal = CarrinhoService.getTotal();

    HTTPGenericService.getGenericURL('app/generics/data/livros.json')
      .success(function(genericResult, status, headers) {
        $scope.livros = genericResult.livros;
      })
      .error(function(err) {
        $scope.messageErro = true;
        $scope.exibeErro("unidades-null", "Unidades");
      });
      
      $scope.adicionaLivro = function(item){
        CarrinhoService.add(item)
        $scope.livrosCarrinhoTotal = CarrinhoService.getTotal();
      }

  }
]);
