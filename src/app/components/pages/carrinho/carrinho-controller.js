'use strict';

angular.module('app').controller('CarrinhoController',
  ['$scope', '$state', 'CarrinhoService',
    function ($scope, $state, CarrinhoService) {
  
      $scope.livros = CarrinhoService.getAll();
      $scope.valorTotal = 0;
      console.log('------------------------------------');
      console.log($scope.livros);
      console.log('------------------------------------');



      $scope.atualizaLivro = function(livro, acrescentaOuDiminui){
        CarrinhoService.update(livro, acrescentaOuDiminui);
      }
      $scope.diminuiLivro = function(livro){
        CarrinhoService.diminui(livro);
      }
      $scope.removeLivro = function(livro){
        CarrinhoService.remove(livro);
      }

      //calcula valor um livro
      $scope.calculaParcial = function (livro) {
        return calculaValorLivro(livro);
      }
      //calcula valor array livros
      $scope.calculaTotal = function () {
        var total = 0;
        angular.forEach($scope.livros, function(livro, key) {
          total += calculaValorLivro(livro); 
        });
        return total;
      }
      //faz conta da quantidade de livros x valor 
      function calculaValorLivro(livro) {
        var totalParcial = 0
        if(livro.contador>1){
          for (let i = 0; i < livro.contador; i++) {
            totalParcial += parseFloat(livro.valor);
          }
        } else {
          totalParcial = parseFloat(livro.valor);
        }
        return totalParcial;
      }

}]);
