'use strict';
angular.module('app').service('CarrinhoService', function($http, $localStorage, _) {
  
  $localStorage = $localStorage.$default({
    produtos: [],
    contadorProduto: 0 // caso precise de busca por id
  });

  var _getAll = function () {
    return $localStorage.produtos;
  };

  var _get = function (produto) {
    return _.where($localStorage.produtos, {titulo: produto.titulo});
  }

  var _add = function (produto) {
    if(_get(produto).length > 0 ){
      _update(produto, true);
    } else {
      produto.id=$localStorage.contadorProduto; // caso precise de busca por id
      produto.contador = 1;
      $localStorage.contadorProduto++;
      $localStorage.produtos.push(produto);
    }
  }
  var _update = function (item, atualiza) {
    var index = _.findIndex($localStorage.produtos, item);
    if (index !== -1) {
      if(atualiza) $localStorage.produtos[index].contador++; 
        else {
          if($localStorage.produtos[index].contador == 1)
            _remove(item)
          else
            $localStorage.produtos[index].contador--; 
        } 
    }
  }

  var _remove = function (item) {
    var index = _.findIndex($localStorage.produtos, item);
    $localStorage.produtos.splice(index, 1);
  }

  var _updateAll = function (produtos) {
    $localStorage.produtos = produtos;
  }

  var _getTotal = function () {
    var total = 0;
    $localStorage.produtos.forEach(function(item) {
      total += item.contador;
    }, this);
    return total;
  }

  

  return {
    getTotal: _getTotal,
    getAll: _getAll,
    get: _get,
    add: _add,
    updateAll: _updateAll,
    update: _update,
    remove: _remove
  };
})
;
