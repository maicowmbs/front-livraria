# Livraria

## Objetivo

Criar um site de vendas para uma livraria de acordo com os mockups nesse documento.
Esse site terá duas páginas: a home e o carrinho.

### Página home

Ela deverá exibir uma lista de livros quaisquer com os seguintes atributos:
- foto
- título
- descrição
- valor

Além disso, cada livro deverá possuir um botão para que uma unidade do livro seja adicionada ao carrinho.

![Home](images/home.PNG)

### Página carrinho

A página do carrinho deverá exibir uma tabela agrupada por livro com os detalhes:
- descrição
- subtotal
- total geral

Deve ser possível remover os livros adicionados no carrinho.

![Home](images/carrinho.PNG)

## Premissas

O site deverá ser um single page app e, consequentemente, não deverá ocorrer perdas de dados (estados) ao navegar entre as páginas.

O layout deve ocupar a tela toda (vertical e horizontalmente) e ser responsivo.

O projeto deve estar "pronto para produção em termos de: estruturação e formatação de código, performance (client/server) e segurança

## Mais informações

Use sua criatividade para melhorar ainda mais e criar coisas interessantes em todos aspectos, principalmente UX.

Lembre-se que queremos ver todo o seu conhecimento e boas práticas neste projeto, portanto cuide do estilo de código, organização do projeto, tomada de decisão, idéias, simplicidade, performance, escalabilidade, robustez, etc.

Envie para nós o link do GitHub ou BitBucket onde está o projeto e o prazo que você irá entregá-lo.
Caso hospede em algum lugar, mande o link do app.



# Livraria Stone

### Estrutura das aplicações

- **/gulp** - Contém os arquivos responsaveis por realizar build da aplicação.

- **/src/app** - Contém a estrutura de pastas da aplicação seguindo arquitetura de aplicações angular modularizadas(Services, Controllers, diretivas).

### Instalando Dependências

Na raiz da aplicação  existem os arquivos **bower.json** e o **package.json**.
O arquivo **bower.json** é responsável por gerenciar as dependências das bibliotecas utilizadas pela Base cliente.
O arquivo **package.json** é responsável por gerenciar as dependências do gerador de pacotes do projeto.

### Instruções

Acesse a raiz do projeto:

Instale as dependências do projeto e de bibliotecas:
```
sh
$ sudo npm install
```
Se não for executado automáticamente pelo npm install, execute também:
```
sh
$ sudo bower install
```
Nota: Atualmente são utilizados nas versões:
- NodeJS - 6.11.0
- NPM - 3.10.10
- Gulp - 3.9.1
- Bower - 1.8.0


### Executando a tarefa de Build
Acesse a raiz do projeto:

Execute os comandos de Build do projeto:
```
sh
$ gulp clean
$ gulp build
```
Todo o build gerado vai criar uma pasta chamada ***/dist*** na raiz do projeto.


##### Base de projeto

O projeto foi baseado no template [sb-admin-2](https://github.com/ethanhann/sb-admin-2).

### Configurações de conexão da API **- Não utilizado nesta versão / utiliza apenas um arquivo na pasta 'app/generics/data/livros.json'**

As configurações de conexão de API estão no arquivo ***app/constants/constants.js*** seguindo o modelo:

### para realizar o build
gulp build --setting {parametro}
onde recebe parametros: producao, homologacao, local


```
angular.module('app')
.constant( 'appConfig', {
  //'siteUrl': '[CONFIG_URL_API]',
  'siteUrl': 'http://200.130.99.108',
  //'apiPath': '[CAMINHO_API]'
  'apiPath': '/intranet/api'
})
```


